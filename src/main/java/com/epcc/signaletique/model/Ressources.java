package com.epcc.signaletique.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ressources")
public class Ressources implements Serializable {
    
	private static final long serialVersionUID = 1L;    
	private List<Ressource> lressource =  new ArrayList<Ressource>();
	
	public Ressources() {
		super();
	}

	public Ressources(List<Ressource> lressource) {
		super();
		this.lressource = lressource;
	}

	public List<Ressource> getLressource() {
		return lressource;
	}

	@XmlElement(name="resource")
	public void setLressource(List<Ressource> lressource) {
		this.lressource = lressource;
	}
	
}
