<html>
	<head>
		<title>Planning </title>
		<style>
			h1 {
				color:#a83e37;
				font-family: Arial;
				font-size:48pt
			}
			h2 {
			    color:#a83e37;
			   	font-family: Arial;
			   	font-size:32pt
			}
			table {
			  border-collapse: collapse;
			  width: 100%;
			}
			
			table, td, th {  
			  border: 1px solid #ddd;
			  text-align: left;
			}

			th, td {
			  padding: 10px;
			  font-family: Arial;
			  font-size: 26pt;
		  	}
		  	
			#logo {
			  display: inline-block;
			  float: left;
			  height: 100px;
			  width: auto; /* correct proportions to specified height */
			  border-radius: 50%; /* makes it a circle */
			}
			.agoraIcon {
		      height: 250px;
		      width: 200px;
		    }
			
		</style>
	</head>
<body>
<!--img src="/image/logo" width="150px" height="200px/-->
	<script>
				
        		var ladate=new Date();
				var month = '' + (ladate.getMonth() + 1);
				var day = '' + ladate.getDate();
				
				if (month.length < 2){
       				 month = '0' + month;
       			}
       			
       			if (day.length < 2) {
       			 	 day = '0' + day;
       			}
       			
       			var datedujour=ladate.getFullYear()+"-"+month+"-"+day;
       			var url='cours/batiment/${batiment}/'+datedujour+"/";
       			//alert("url = "+url);
    		</script>
		<#-- affichage des 5 premiers cours -->
		<#if totalCours gt 0 && totalCours lte 10 >
		
			<div style="height:850px">
				<table>
					<caption><h1>${batiment}</h1><h2>${coursOuEvenement} du : ${dateduJour} </h2></caption>
					<tr>
						<th>Horaires</th>
						<th>Salle</th>
						<!-- TODO th style width 50% et faire un un truncate de l'intitulé -->
						<th>Etablissement</th>
						<th width="50%">Intitulé</th>
					</tr>
				
					<#list lcours as acours>
						<tr>
							<td>${acours.heurDebutCours} - ${acours.setHeurFinCours}</td>
							<td>${acours.infoSalle}</td>
							<td>${acours.formationNom}</td>
							<td>${acours.nomActivite}</td>
							
						</tr>
					</#list>
			
				</table>
			</div>
	
		<#elseif totalCours gt  10>
	
			<span id="page1"></span>
			<div style="height:1000px">
				<table>
					<caption><h1>Planning ${batiment}</h1><h2>${coursOuEvenement} du : ${dateduJour}  </h2></caption>
					<tr>
						<th>Horaires</th>
						<th>Salle</th>
						<th>Etablissement</th>
						<th>Intitulé</th>
						
					</tr>
					<#assign seq = lcours[0..9]>
					<#list seq as acours>
							<tr>
								<td>${acours.heurDebutCours} - ${acours.setHeurFinCours}</td>
								<td>${acours.infoSalle}</td>						
								<td>${acours.formationNom}</td>
								<td>${acours.nomActivite}</td>
								
							</tr>
		  				
					</#list>
				</table>
			</div>
			<span id="page2"></span>
			<div style="height:1000px">
				<table>
					<tr>
						<th>Horaires</th>
						<th>Salle</th>
						<th>Etablissement</th>
						<th>Intitulé</th>
						
					</tr>
					<#assign seq = lcours[10..]>
					<caption> : <h1>Suite : planning ${batiment}</h1><h2>(${coursOuEvenement} du : ${dateduJour} ) </h2></caption>
					<#list seq as acours>
							<tr>
								<td>${acours.heurDebutCours} - ${acours.setHeurFinCours}</td>
								<td>${acours.infoSalle}</td>
								<td>${acours.formationNom}</td>
								<td>${acours.nomActivite}</td>
								
							</tr>
		  				
					</#list>
				</table>
			</div>
		<#else>
			<span id="page1"></span>
				<div class="center">
					<table>
						<caption>
							<h1>${batiment}</h1>
							<h2>${coursOuEvenement} du : ${dateduJour} </h2>
							<h2> Il n' y a pas de cours ou événement pour l'instant !!! </h2>
						</caption>
					</table>
				</div>
		</#if>
	</body>
</html>
