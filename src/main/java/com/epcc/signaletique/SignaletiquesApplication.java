package com.epcc.signaletique;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;;

@SpringBootApplication
public class SignaletiquesApplication extends SpringBootServletInitializer{

	private static final Logger logger = LoggerFactory.getLogger(SignaletiquesApplication.class);
	
	public static void main(String[] args) {
		logger.info("start application");
		SpringApplication.run(SignaletiquesApplication.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SignaletiquesApplication.class);
	}

}
