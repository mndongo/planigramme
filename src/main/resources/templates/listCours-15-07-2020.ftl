<html>
	<head>
		<title>Planning </title>
		<style>
			table, td, th {  
			  border: 1px solid #ddd;
			  text-align: left;
			}

			table {
			  border-collapse: collapse;
			  width: 100%;
			}

			th, td {
			  padding: 10px;
			  font-family: Arial; font-size: 10pt;
		  	}
		  	
			
		</style>
	</head>
	<body>
		<script>
				
        		var ladate=new Date();
				var month = '' + (ladate.getMonth() + 1);
				var day = '' + ladate.getDate();
				
				if (month.length < 2){
       				 month = '0' + month;
       			}
       			
       			if (day.length < 2) {
       			 	 day = '0' + day;
       			}
       			
       			var datedujour=ladate.getFullYear()+"-"+month+"-"+day;
       			var url='cours/batiment/${batiment}/'+datedujour+"/";
       			//alert("url = "+url);
    		</script>
		<#-- affichage des 5 premiers cours -->
		<#if totalCours gt 0 && totalCours lte 5 >
		
			<div style="height:850px">
				<table>
					<caption><h1>${batiment}</h1><h2>Planning du : ${dateduJour} ${matinOuApresMidi} </h2></caption>
					<tr>
						<th>Horaires</th>
						<th>Salle</th>
						<th>Etablissement</th>
						<th>Intitulé</th>
					</tr>
				
					<#list lcours as acours>
						<tr>
							<td>${acours.heurDebutCours}-${acours.endHour}</td>
							<td>${acours.infoSalle}</td>
							<td>${acours.formationNom}</td>
							<td>${acours.nomActivite}</td>
						</tr>
					</#list>
			
				</table>
			</div>
	
		<#elseif totalCours gt  5>
	
			<span id="page1"></span>
			<div style="height:850px">
				<table>
					<caption><h1>${batiment}</h1><h2>Planning du : ${dateduJour} ${matinOuApresMidi}</h2></caption>
					<tr>
						<th>Horaires</th>
						<th>Salle</th>
						<th>Etablissement</th>
						<th>Intitulé</th>
					</tr>
					<#assign seq = lcours[0..4]>
					<#list seq as acours>
						<tr>
							<td>${acours.heurDebutCours}-${acours.endHour}</td>
							<td>${acours.infoSalle}</td>
							<td>${acours.formationNom}</td>
							<td>${acours.nomActivite}</td>
						</tr>
  					</#list>
  				
				</table>
			</div>
			<span id="page2"></span>
			<div style="height:850px">
				<table>
					<caption><h1>${batiment}</h1><h2>Planning du : ${dateduJour} ${matinOuApresMidi}</h2></caption>
					<tr>
						<th>Horaires</th>
						<th>Salle</th>
						<th>Etablissement</th>
						<th>Intitulé</th>
					</tr>
				
					<#assign seq = lcours[5..]>
					<#list seq as acours>
						<tr>
							<td>${acours.heurDebutCours}-${acours.endHour}</td>
							<td>${acours.infoSalle}</td>
							<td>${acours.formationNom}</td>
							<td>${acours.nomActivite}</td>
						</tr>
					</#list>
			
				</table>
			</div>
		<#else>
		<span id="page1"></span>
			<div class="center">
				<table>
					<caption>
						<h1>${batiment}</h1>
						<h2>Planning du : ${dateduJour} ${matinOuApresMidi}</h2>
						<h2> Il n' y a pas de cours pour l'instant !!! </h2>
					</caption>
				</table>
			</div>
		</#if>
		<a href="#page1"></a><br>
		<a href="#page2"></a>
		<a href="#page3"></a>
		<script type="text/javascript">
			function formatDate() {
			   alert("function formatDate test");
			}
		</script>
	</body>
	
</html>
