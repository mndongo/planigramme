package com.epcc.signaletique.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epcc.signaletique.service.SignaletiqueService;

@RestController
@RequestMapping(path="/ade")
public class SignaletiqueController {
	private static final Logger logger = LoggerFactory.getLogger(SignaletiqueController.class);
	
	private SignaletiqueService signaletiqueService;

	
	public SignaletiqueController(SignaletiqueService signaletiqueService) {
		super();
		this.signaletiqueService = signaletiqueService;
	}
	
	@GetMapping(path="/{events}/batiment/{batName}/{date}")
	public ResponseEntity<String> getListCours(@PathVariable("events") String events,@PathVariable("batName") String name, @PathVariable("date") String date){
		logger.info(" && VieCampusController - getListcours");
		logger.info("param is : "+name);
	    String builder =  signaletiqueService.getHtmlListCours(name,date,events);
	    
		return ResponseEntity.ok(builder.toString());
	
	}

	@GetMapping(path="/")
	public String index(){
		logger.info(" && VieCampusController - page d'acceuil");
		return "page d'acceuil";
	
	}
}
