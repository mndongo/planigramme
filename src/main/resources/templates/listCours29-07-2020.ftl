<#import "/spring.ftl" as spring/>
<html>
	<head>
		<title>Planning </title>
		
		<style>
			h1 {
				color:#a83e37;
				font-family: Arial;
				font-size:48pt
			}
			h2 {
			    color:#a83e37;
			   	font-family: Times New Roman;
			   	font-size:32pt
			}
			table {
			  border-collapse: collapse;
			  width: 100%;
			}
			
			table, td, th {  
			  border: 1px solid #ddd;
			  text-align: left;
			}

			th, td {
			  padding: 10px;
			  font-family: Arial;
			  font-size: 18pt;
		  	}
		  	
			#logo {
			  display: inline-block;
			  float: left;
			  height: 100px;
			  width: auto; /* correct proportions to specified height */
			  border-radius: 50%; /* makes it a circle */
			}
			.agoraIcon {
		      height: 250px;
		      width: 200px;
		    }
			
		</style>
	<body>
		<!--img src="/image/logo" width="150px" height="200px/-->
		<script>
				
        		var ladate=new Date();
				var month = '' + (ladate.getMonth() + 1);
				var day = '' + ladate.getDate();
				
				if (month.length < 2){
       				 month = '0' + month;
       			}
       			
       			if (day.length < 2) {
       			 	 day = '0' + day;
       			}
       			
       			var datedujour=ladate.getFullYear()+"-"+month+"-"+day;
       			var url='cours/batiment/${batiment}/'+datedujour+"/";
       			//alert("url = "+url);
    		</script>
		<#-- affichage des 5 premiers cours -->
		
			<div style="height:850px">
				<table>
					<caption><h1>Planning ${batiment}</h1><h2>${coursOuEvenement} du : ${dateduJour}  </h2></caption>
					<tr>
						<th>Horaires</th>
						<th>Intitulé</th>
						<th>Etablissement</th>
						<th>Salle</th>
						
					</tr>
				
					<#list lcours as acours>
						<tr>
							<td>${acours.heurDebutCours}-${acours.setHeurFinCours}</td>
							<td>${acours.nomActivite}</td>							
							<td>${acours.formationNom}</td>
							<td>${acours.infoSalle}</td>
						</tr>
					</#list>
			
				</table>
			</div>
		<script type="text/javascript">
			function formatDate() {
			   alert("function formatDate test");
			}
		</script>
	</body>
	
</html>
