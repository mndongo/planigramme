package com.epcc.signaletique.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.epcc.signaletique.model.Cours;
import com.epcc.signaletique.model.Event;
import com.epcc.signaletique.model.EventsEAC;
import com.epcc.signaletique.model.Ressource;
import com.epcc.signaletique.model.Session;
import com.epcc.signaletique.writer.FreemarkerTemplateWriter;


@Service
public class SignaletiqueService {

	@Value("${spring.ade.function}")
	private String adeFunction;
	@Value("${spring.ade.login}")
	private String adeLogin;
	@Value("${spring.ade.password}")
	private String adePassword;
	@Value("${spring.ade.path}")
	private String adePath;
	@Value("${spring.ade.idProjet}")
	private int idProjet;
	@Value("${spring.ade.function.initProject}")
	private String initProject;
	@Value("${spring.ade.functon.eventFunction}")
	private String eventsfunction;
	@Value("${spring.ade.functon.activitiesFunction}")
	private String activitiesFunction;
	@Value("${rci1.salles}")
	private String listSalleRci1;
	@Value("${spring.cours.matin}")
	private String coursMatin;
	@Value("${spring.cours.soir}")
	private String coursSoir;
	@Value("${spring.batiment.rci1}")
	private String nameBatRci1;
	@Value("${spring.batiment.rci3}")
	private String nameBatRci3;
	@Value("${spring.batiment.eac}")
	private String nameBatEac;
	@Value("${spring.batiment.epcc}")
	private String nameBatEpcc;
	@Value("${spring.batiment.ccoll}")
	private String nameBatCcoll;
	@Value("${spring.batiment.ined}")
	private String nameBatIned;
	@Value("${spring.idRci1}")
	private String idRCi1;
	@Value("${spring.idRci3}")
	private String idRCi3;
	@Value("${spring.idCcoll}")
	private String idCcoll;
	@Value("${spring.idRci1}")
	private String idIned;
	@Value("${spring.idIned}")
	private String idEac;
	@Value("${spring.idEac}")
	private String idEpcc;
	@Value("${spring.idEpcc}")

	private static final Logger logger = LoggerFactory.getLogger(SignaletiqueService.class);
	
	@Autowired
	FreemarkerTemplateWriter freemarker;
	
	@Autowired
	RestTemplate restempRestTemplateAde;
	
	/*@Autowired
    private Environment env;*/
	
	public SignaletiqueService() {
	}

	@Cacheable(value = "getHtmlListCours")
	public String getHtmlListCours(String name,String date,String events){
		
		/* logger.info("From Environment");
	     logger.info("Application name: {}", env.getProperty("app.name"));
	     logger.info("Application version: {}", env.getProperty("app.version"));*/
	        
		String id ="";
		String typeEvents="";
		StringBuilder sbdate= new StringBuilder();
		String mois = date.substring(5).substring(0, 3);
		String jour = date.substring(8,10);
		String an = date.substring(0, 4);
		sbdate.append(mois);
		sbdate.append(jour);
		sbdate.append("-");
		sbdate.append(an);
		
		String reversedate= sbdate.toString();
		StringBuilder dateFrenchFormat = new StringBuilder();
		dateFrenchFormat.append(jour);
		dateFrenchFormat.append("-");
		dateFrenchFormat.append(mois);
		dateFrenchFormat.append(an);
		
		logger.info("date ev= "+date);
		String matinOuApresMidi="";
		try {	
			if("RCI1".equals(name)){
				id=idRCi1;
				name=nameBatRci1;
			}else if("RCI3".equals(name)){
				id=idRCi3;
				name=nameBatRci3;
			}else if("CCOLL".equals(name)){
				id=idCcoll;
				name=nameBatCcoll;
			}else if("INED".equals(name)){
				id=idIned;
				name=nameBatIned;
			}else if("EAC".equals(name)){
				id=idEac;
				name=nameBatEac;
			}else if("EPCC".equals(name)){
				id=idEpcc;
				name=nameBatEpcc;
				
			}
			
			
		}catch (Exception e) {
			// TODO: handle exception
			logger.error("erreur :" +e.getMessage());
		}
		final HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_VALUE);
		final HttpEntity headersEntity = new HttpEntity<>(headers);
		String uri = adePath+"&function="+adeFunction+"&login="+adeLogin+"&password="+adePassword;
		final ResponseEntity<Session> response = restempRestTemplateAde.exchange(uri, HttpMethod.GET, headersEntity, Session.class);
		
		String sessionId = response.getBody().getId();
		initProject(sessionId, idProjet);
		// récupération des cours après initialisation du projet 
		File fileEvent = getEventFile(sessionId,id,reversedate);
		List<Cours> lcours = parseFile(fileEvent,name);
		
		LocalDateTime currentTime = LocalDateTime.now();
		List<Cours> listCourDuMatinOuSoir = new ArrayList<Cours>();
		/*logger.info("heure now = "+currentTime.getHour());
		logger.info("minute now = "+currentTime.getMinute());
		logger.info("heure now + 2 = "+(currentTime.getHour()+2));*/
		// récupération des cours non terminés
		listCourDuMatinOuSoir  = getCoursNonTermine(lcours);
		// récupération des cours ou évenements
		List<Cours> listCours = listCourDuMatinOuSoir.
				stream().filter(x->events.equals(x.getCateg())).collect(Collectors.toList());;
		
				listCours.sort(Comparator.comparing(Cours::getStartHour)
	                .thenComparing(Cours::getStartHour)
	                .thenComparing(Cours::getEndHour));
		if("evenement".equals(events)) {
			typeEvents="événement(s)";
		}else {
			typeEvents=events;
		}
		return freemarker.writeHtmlListCours(listCours,dateFrenchFormat.toString(),name,typeEvents);
	}

	
	/**
	 * initialisation du projet
	 */
	public void initProject(String session,int idproject) {
		final HttpHeaders headers = new HttpHeaders();
		//MediaType mediaType = new MediaType("application", "xml", StandardCharsets.ISO_8859_1);
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_VALUE);
		//headers.setContentType(mediaType);
		final HttpEntity headersEntity = new HttpEntity<>(null,headers);
		
		String uri = adePath+"&sessionId="+session+"&function="+initProject+"&projectId="+idproject;
		logger.info(" *** uri setprojet = "+uri);
		restempRestTemplateAde.getMessageConverters()
        .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		final ResponseEntity<String> response = restempRestTemplateAde.exchange(uri, HttpMethod.GET, headersEntity, String.class);
		logger.info(" *** status init projet  = "+response.getStatusCode());
	}
	
	public File getEventFile(String session,String idResource,String dateEvent){
		logger.info("date event = "+dateEvent);
		String date = dateEvent.replace("-", "/");
		logger.info("date parsing = "+date);
		logger.info("id res = "+idResource);
		// récupération de la date du jour
		final HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_VALUE);
		headers.setAcceptCharset(Arrays.asList(Charset.forName("ISO-8859-1")));
		final HttpEntity headersEntity = new HttpEntity<>(null,headers);
		/**
		 * TODO
		 *  la date et la ressource doivent être passées en param 
		 *  gerer la ressource dans un map
		 *  gerer le cahce 
		 */
		String uri = adePath+"&sessionId="+session+"&function="+eventsfunction+"&resources="+idResource+"&date="+date+"&detail=8";
		final ResponseEntity<String> response = restempRestTemplateAde.exchange(uri, HttpMethod.GET, headersEntity, String.class);
		logger.info(" *** status event projet  = "+response.getStatusCode());
		StringBuilder builder = new StringBuilder(response.getBody());
		File file = new File("src/main/resources/events/"+date.replace("/","-")+"_eventsFile.xml");
		// TODO à mettre dans une variable ou constante
		//File file = new File("/opt/viedecampus"+date.replace("/","-")+"_eventsFile.xml");
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
		    writer.write(builder.toString());
		}catch (IOException e) {
			// TODO gerer le type d'erreur
			e.printStackTrace();
		}
		return file;
	}
	
	/**
	 * 
	 * @param f
	 */
	private List<Cours> parseFile(File f,String batName) {
		List<Cours> listCours = new ArrayList<Cours>();
		try {
			
			if(f.exists()) {
				logger.info("file is created");
				JAXBContext jaxbContext;
				try
				{
					jaxbContext = JAXBContext.newInstance(EventsEAC.class);              
					Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					EventsEAC eventsEAC = (EventsEAC) jaxbUnmarshaller.unmarshal(f);
					for(Event e : eventsEAC.getlEvent()) {
						Cours cours = new Cours();
						List<String> numerosDesSalle = new ArrayList<String>();
						List<String> listFormation = new ArrayList<String>();
						String str2 = new String(e.getName().getBytes(),Charset.forName("ISO-8859-1"));
						cours.setFormationNom("-");
						cours.setInfoSalle("-");
						cours.setNomActivite(e.getName());
						//cours.setStartHour(e.getStartHour());
						if(e.getStartHour().length()>3) {
							String heurematin = e.getStartHour().substring(0, 2);
							cours.setStartHour(heurematin);
						}else {
							cours.setStartHour(e.getStartHour());						
						}
						
						cours.setHeurDebutCours(e.getStartHour());
						
						
						if(e.getEndHour().length()>3) {
							String heureapremidi = e.getEndHour().substring(0, 2);
							String minute = e.getEndHour().substring(3, 5);
							cours.setEndHour(heureapremidi);
							cours.setMinute(minute);
						}else {
							cours.setEndHour(e.getStartHour());						
						}
						
						cours.setSetHeurFinCours(e.getEndHour());
						cours.setDate(e.getDate());
						cours.setBatimentName(batName);
						// nbre de resource pour cet event  
						List<Ressource> resourceListe = e.getRessources().getLressource();
						//logger.info("nbre de ressource pour cet event = "+resourceListe.size());
						for(Ressource res : resourceListe) {
							//logger.info("ressource quantity = "+res.getQuantity());
							// TODO on ne teste que categorie 5 et trainee
							if(res.getCategory().equalsIgnoreCase("category5") ){
								cours.setFormationNom(res.getName());
								cours.setCateg(res.getCategory());
								//cours.setInfoSalle(res.getName());
								//numerosDesSalle.add(res.getName());
								listFormation.add(res.getName());
								cours.setCateg("evenement");
								
							}else 
							if(res.getCategory().equalsIgnoreCase("trainee") ){
								cours.setFormationNom(res.getName());
								cours.setCateg(res.getCategory());
								//cours.setInfoSalle(res.getName());
								//numerosDesSalle.add(res.getName());
								listFormation.add(res.getName());
								cours.setCateg("cours");
								
							}
							if(res.getCategory() != null && res.getCategory().equalsIgnoreCase("classroom")){
								cours.setInfoSalle(res.getName());
								numerosDesSalle.add(res.getName());
								
							}
					    }
						if(listFormation.size()>1) {
							StringBuilder builder = new StringBuilder();
							for(String form : listFormation) {
								builder.append(form);
								builder.append(" | ");
							} 
							cours.setFormationNom(builder.toString());
						}
						if(numerosDesSalle.size()>1) {
							StringBuilder builder = new StringBuilder();
							for(String num : numerosDesSalle) {
								builder.append("");
								builder.append(num);
								builder.append("/ ");
							}
							cours.setInfoSalle(builder.toString());
						}listCours.add(cours);
					}
				}
				catch (JAXBException e) 
				{
				    e.printStackTrace();
				}

			}else {
				logger.error("could not created");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return listCours;
		
	}
	
	private List<Cours>getHeureCours(List<Cours> allcours,boolean ismorning) {
		List<Cours> coursMatin = new ArrayList<Cours>();
		List<Cours> coursSoir = new ArrayList<Cours>();
		//récupération des cours du matin
		//ismorning=true;
		for(Cours cours : allcours) {
			if(Integer.valueOf(cours.getStartHour()) < 12) {
				coursMatin.add(cours);
			}else {
				coursSoir.add(cours);
			}
		}
		logger.info("cours du matin: "+coursMatin.size());
		logger.info("cours du soir: "+coursSoir.size());
		if(ismorning) {
			return coursMatin;
		}else {
			return coursSoir;
		}
	}
	
	private List<Cours> getCoursNonTermine(List<Cours> lcours) {
		LocalDateTime currentTime = LocalDateTime.now();
		//List<Cours> coursMatin = new ArrayList<Cours>();
		Set<Cours> coursMatin = new HashSet<Cours>();
		//logger.info("** now  = "+currentTime.getHour());
		//logger.info("## now + 2 = "+(currentTime.getHour()+2));
		// ajout de cours qu débute dans finisse à l'instant
		/*Cours cours1 = new Cours();
		cours1.setStartHour("08");
		cours1.setEndHour("11");
		cours1.setHeurDebutCours("08:00");
		cours1.setSetHeurFinCours("11:30");
		cours1.setMinute("30");
		cours1.setFormationNom("formation paris 1");
		cours1.setNomActivite("histoire du monde cours 1");
		cours1.setInfoSalle("0.125");
		cours1.setCateg("evenement");
		
		Cours cours2 = new Cours();
		cours2.setStartHour("09");
		cours2.setEndHour("11");
		cours2.setHeurDebutCours("09:00");
		cours2.setSetHeurFinCours("11:35");
		cours2.setMinute("35");
		cours2.setFormationNom("paris 8");
		cours2.setNomActivite("histoire du tama cours 2");
		cours2.setInfoSalle("0.126");
		cours2.setCateg("cours");
		lcours.add(cours1);
		lcours.add(cours2);
		
		// ajout des cours qui commence dans moins de 2h
		Cours cours3 = new Cours();
		cours3.setStartHour("10");
		cours3.setEndHour("12");
		cours3.setHeurDebutCours("10:00");
		cours3.setSetHeurFinCours("12:30");
		cours3.setMinute("30");
		cours3.setFormationNom("formation paris 1");
		cours3.setNomActivite("militantisme du moment cours 3");
		cours3.setInfoSalle("0.127");
		cours3.setCateg("evenement");
		
		Cours cours4 = new Cours();
		cours4.setStartHour("17");
		cours4.setEndHour("19");
		cours4.setHeurDebutCours("17:00");
		cours4.setSetHeurFinCours("19:30");
		cours4.setMinute("30");
		cours4.setFormationNom("paris 8");
		cours4.setNomActivite("initiatives positives cours 4");
		cours4.setInfoSalle("0.128");
		cours4.setCateg("cours");
		
		lcours.add(cours3);
		lcours.add(cours4);
		
		Cours cours5 = new Cours();
		cours5.setStartHour("15");
		cours5.setEndHour("18");
		cours5.setHeurDebutCours("15:00");
		cours5.setSetHeurFinCours("18:00");
		cours5.setMinute("00");
		cours5.setFormationNom("paris 8");
		cours5.setNomActivite("droits des femmes cours 5");
		cours5.setInfoSalle("0.129");
		cours5.setCateg("evenement");
		
		Cours cours6 = new Cours();
		cours6.setStartHour("12");
		cours6.setEndHour("16");
		cours6.setHeurDebutCours("12:00");
		cours6.setSetHeurFinCours("16:59");
		cours6.setMinute("59");
		cours6.setFormationNom("paris 8");
		cours6.setNomActivite("coeurs des hommes cours 6");
		cours6.setInfoSalle("0.129");
		cours6.setCateg("cours");
		
		lcours.add(cours5);
		lcours.add(cours6);
		
		Cours cours7 = new Cours();
		cours7.setStartHour("13");
		cours7.setEndHour("16");
		cours7.setHeurDebutCours("13:00");
		cours7.setSetHeurFinCours("16:15");
		cours7.setMinute("15");
		cours7.setFormationNom("paris 8");
		cours7.setNomActivite("cours 7 passion primaire");
		cours7.setInfoSalle("0.130");
		cours7.setCateg("evenement");
		
		Cours cours8 = new Cours();
		cours8.setStartHour("14");
		cours8.setEndHour("15");
		cours8.setHeurDebutCours("14:00");
		cours8.setSetHeurFinCours("15:45");
		cours8.setMinute("45");
		cours8.setFormationNom("paris 8");
		cours8.setNomActivite("rfi midi cours 8");
		cours8.setInfoSalle("0.131");
		cours8.setCateg("cours");
		
		Cours cours9 = new Cours();
		cours9.setStartHour("15");
		cours9.setEndHour("17");
		cours9.setHeurDebutCours("15:00");
		cours9.setSetHeurFinCours("17:30");
		cours9.setMinute("30");
		cours9.setFormationNom("paris 8");
		cours9.setNomActivite("histoire des hommes cours 9");
		cours9.setInfoSalle("0.132");
		cours9.setCateg("evenement");
		
		Cours cours10 = new Cours();
		cours10.setStartHour("18");
		cours10.setEndHour("22");
		cours10.setHeurDebutCours("18:00");
		cours10.setSetHeurFinCours("22:40");
		cours10.setMinute("40");
		cours10.setFormationNom("paris 8");
		cours10.setNomActivite("relation Chine Usa cours 10");
		cours10.setInfoSalle("0.133");
		cours10.setCateg("cours");
	
		Cours cours11 = new Cours();
		cours11.setStartHour("19");
		cours11.setEndHour("21");
		cours11.setHeurDebutCours("19:00");
		cours11.setSetHeurFinCours("21:30");
		cours11.setMinute("30");
		cours11.setFormationNom("ugb");
		cours11.setNomActivite("relation de l'économie française cours 11");
		cours11.setInfoSalle("0.133");
		cours11.setCateg("cours");
		
		Cours cours12 = new Cours();
		cours12.setStartHour("20");
		cours12.setEndHour("22");
		cours12.setHeurDebutCours("20:00");
		cours12.setSetHeurFinCours("22:15");
		cours12.setMinute("15");
		cours12.setFormationNom("ucad");
		cours12.setNomActivite("économie verte dans la région cours 12");
		cours12.setInfoSalle("0.133");
		cours12.setCateg("evenement");
		
		lcours.add(cours7);
		lcours.add(cours8);
		lcours.add(cours9);
		lcours.add(cours10);
		lcours.add(cours11);
		lcours.add(cours12);*/
		
		for(Cours cours : lcours) {
			// heure de fin du cours <= à heure actuel (now)
			/*logger.info("-- heure debut du cours  "+cours.getFormationNom() +" débute à : "+cours.getStartHour()+" h ");
			logger.info("-- heure fin du cours  "+cours.getFormationNom() +" fini à : "+cours.getEndHour()+" h");
			logger.info("** minute fin = " +cours.getMinute());
			logger.info("intitulé du cours = "+cours.getNomActivite());*/
			if(cours.getNomActivite().length() > 30 ) {
				String intituletrunc = cours.getNomActivite().substring(0, 30).concat("...");
				cours.setNomActivite(intituletrunc);
			}
			// pas d'affichage des cours terminés (cours commencé et terminé)
			if(Integer.valueOf(cours.getEndHour()) <= (currentTime.getHour())){
				//  on regarde si le cours est toujours en cours
				if(Integer.valueOf(cours.getEndHour()) == currentTime.getHour() 
					&& Integer.valueOf(cours.getMinute()) >= (currentTime.getMinute())) {
					coursMatin.add(cours);
					
				}
				// si le cours fini dans les 2h à venir
				else if(Integer.valueOf(cours.getEndHour()) <= (currentTime.getHour()+2 ) 
						||  Integer.valueOf(cours.getMinute()) <= (currentTime.getMinute()+60))
						{
					logger.info("pas d'affichage du cours");	
					//logger.info("test");
				}else {
					
				}
				
			}
			// cours qui débutent dans moins de 2h
			else if((Integer.valueOf(cours.getStartHour()) >= (currentTime.getHour())
					&& (Integer.valueOf(cours.getStartHour()) <= (currentTime.getHour()+2)))) {
				coursMatin.add(cours);
				
			}
			// cours débuté mais non terminé encore
			else if ((Integer.valueOf(cours.getStartHour()) <= (currentTime.getHour())
					&& (Integer.valueOf(cours.getEndHour()) >= (currentTime.getHour())))) {
				coursMatin.add(cours);
				
			}else {
				logger.info("cours debute dans plus de 2h");
				
			}
			// tous les cours commencés et non terminés
			// && (Integer.valueOf(cours.getEndHour()) <= (currentTime.getHour()+2))
/*			if(Integer.valueOf(cours.getStartHour()) <= (currentTime.getHour()) && 
					Integer.valueOf(cours.getMinute()) <= (currentTime.getMinute())){
					logger.info("add cours : "+cours.getFormationNom());
				
			}*/
			// cours qui débutent dans moins de 2h
			
			/*if((Integer.valueOf(cours.getStartHour()) > (currentTime.getHour()) && (Integer.valueOf(cours.getStartHour()) <= (currentTime.getHour()+2)))) {
				logger.info("add cours : "+cours.getFormationNom());
				coursMatin.add(cours);
			}*/
		}
		logger.info("size cours à afficher = "+coursMatin.size());
		List<Cours> lesCours = new ArrayList<Cours>();
		Iterator<Cours> it = coursMatin.iterator();
		while(it.hasNext()) {
			lesCours.add(it.next());
		}
		return lesCours;
	}

}
