package com.epcc.signaletique.config;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.epcc.signaletique.writer.FreemarkerTemplateWriter;

public class RestTemplateConfiguration {

	@Value("${rest.client.read.timeout.ms:5000}")
    private int readTimeout;

    @Value("${rest.client.connect.timeout.ms:5000}")
    private int connectTimeout;

    /**
     * Construction du client REST.
     * Attention, le certificat intermédiaire n'est pas présent dans la configuration SSL de l'environnement de test :
     * le code suivant permet de désactiver le contrôle de la validité du certificat. Il faudra désactiver ce code en production.
     *
     * @return
     */
    @Bean
    public RestTemplate restTemplate() {
        final HttpComponentsClientHttpRequestFactory requestFactory = generateRestTemplateFactory();

        return new RestTemplate(requestFactory);
    }


    /**
     * Construction du client REST spécifique a ADE (avec un interceptor permettant de traiter toutes les réponse en tant que XML.)
     * Attention, le certificat intermédiaire n'est pas présent dans la configuration SSL de l'environnement de test :
     * le code suivant permet de désactiver le contrôle de la validité du certificat. Il faudra désactiver ce code en production.
     *
     * @return
     */
    @Bean
    @Primary
    public RestTemplate restTemplateAde() {
        final HttpComponentsClientHttpRequestFactory requestFactory = generateRestTemplateFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        restTemplate.getInterceptors().add((request, body, execution) -> {
            ClientHttpResponse response = execution.execute(request,body);
            response.getHeaders().setContentType(MediaType.APPLICATION_XML);
            return response;
        });
        return restTemplate;
    }

    /**
     * Code commun aux deux générations de rest Template : génération de la factory.
     * @return
     */
    private HttpComponentsClientHttpRequestFactory generateRestTemplateFactory() {
        final TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
        final SSLContext sslContext;
        try {
            sslContext = org.apache.http.ssl.SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            throw new IllegalStateException(e);
        }
        final SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
        final CloseableHttpClient httpClient =
            HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();
        final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        requestFactory.setReadTimeout(readTimeout);
        requestFactory.setConnectTimeout(connectTimeout);
        return requestFactory;
    }
    
    @Bean
	public FreeMarkerConfigurer freeMarkerConfigurer(){
		//logger.info("+++++ bean freeMarkerConfigurer +++++");
		FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
		freeMarkerConfigurer.setTemplateLoaderPath("classpath:/templates"); 
		freeMarkerConfigurer.setDefaultEncoding("UTF-8");
		return freeMarkerConfigurer;
	}

	@Bean
	public FreemarkerTemplateWriter freemarkerTemplateWriter(){
		return new FreemarkerTemplateWriter();
	}

}
