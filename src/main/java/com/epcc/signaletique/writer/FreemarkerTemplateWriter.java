package com.epcc.signaletique.writer;

import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.epcc.signaletique.model.Cours;

import freemarker.template.Template;

public class FreemarkerTemplateWriter {

	private static final Logger logger = LoggerFactory.getLogger(FreemarkerTemplateWriter.class);

	public FreemarkerTemplateWriter() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	private FreeMarkerConfigurer freeMarkerConfigurer;
	
	public String writeHtmlListCours(List<Cours> lcours,String dateDuJour,String batiment,String events){
		logger.info("start writeHtmlListCours");
		logger.info("nbre total de cours = "+lcours.size());
		try {
			Map<String, Object> data = new HashMap();
			//TODO afficher l'heure
			// modifier l'heure de fin mettre exemple 13:00
			data.put("lcours", lcours);
			data.put("dateduJour", dateDuJour);
			data.put("batiment", batiment);
			data.put("coursOuEvenement", events);
			data.put("totalCours", lcours.size());
			Template template = freeMarkerConfigurer.getConfiguration().getTemplate("listCours.ftl");
			
			StringWriter sw = new StringWriter();
			template.process(data, sw);
			
			logger.info("Writing of html file completed successfully!");
			
			return sw.toString();
		}catch(Exception e) {
			logger.error("File writing using the Freemarker template engine failed!", e);
		}
		logger.info("finish writeHtmlListCours");
		return null;

	}

}
