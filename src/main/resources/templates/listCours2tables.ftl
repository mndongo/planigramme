<html>
	<head>
		<title>Planning </title>
		<style>
			table, td, th {  
			  border: 1px solid #ddd;
			  text-align: left;
			}

			table {
			  border-collapse: collapse;
			  width: 100%;
			}

			th, td {
			  padding: 10px;
			  font-family: Arial; font-size: 10pt;
		  	}
		  	
			
		</style>
	</head>
	<body>
<a href="#page1"></a><br>
<a href="#page2"></a>

		<span id="page1"></span>
		<div style="height:850px">
			<table>
				<caption><h1>${batiment}</h1><h2>Planning du : ${dateduJour} ${matinOuApresMidi} : ${totalCours} Cours </h2></caption>
				<tr>
					<th>Horaires</th>
					<th>Salle</th>
					<th>Etablissement</th>
					<th>Intitulé</th>
				</tr>
			
				<#assign seq = lcours[1..4]>
				<#list seq as acours>
					<tr>
						<td>${acours.heurDebutCours}-${acours.endHour}</td>
						<td>${acours.infoSalle}</td>
						<td>${acours.formationNom}</td>
						<td>${acours.nomActivite}</td>
					</tr>
  				
				</#list>
				
			</table>
		</div>
		
		<span id="page2"></span>
		<div style="height:850px">
			<table>
				<tr>
					<th>Horaires</th>
					<th>Salle</th>
					<th>Etablissement</th>
					<th>Intitulé</th>
				</tr>
				<#assign seq = lcours[5..]>
				<#list seq as acours>
					<tr>
						<td>${acours.heurDebutCours}-${acours.endHour}</td>
						<td>${acours.infoSalle}</td>
						<td>${acours.formationNom}</td>
						<td>${acours.nomActivite}</td>
					</tr>
  				</#list>
			</table>
		</div>
	</body>
</html>
