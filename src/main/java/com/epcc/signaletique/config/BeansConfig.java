package com.epcc.signaletique.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.epcc.signaletique.writer.FreemarkerTemplateWriter;

@Configuration
@PropertySource(value="file:C:\\Users\\xxxx\\app.properties")
//@PropertySource(value="file:/opt/signaletique/application.properties")
//@PropertySource("file:${:\\opt\\signaletique}\\application.properties")
public class BeansConfig {

private final static Logger logger = LoggerFactory.getLogger(BeansConfig.class);
	
	@Bean
	public FreeMarkerConfigurer freeMarkerConfigurer(){
		logger.info("+++++ bean freeMarkerConfigurer +++++");
		FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
		freeMarkerConfigurer.setTemplateLoaderPath("classpath:/templates"); 
		freeMarkerConfigurer.setDefaultEncoding("UTF-8");
		return freeMarkerConfigurer;
	}

	@Bean
	public FreemarkerTemplateWriter freemarkerTemplateWriter(){
		return new FreemarkerTemplateWriter();
	}
	
	@Bean
	RestTemplate restTemplateAde() {
		logger.info("----- bean restTemplateAde ----");
		final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		
		 restTemplate.getInterceptors().add((request, body, execution) -> {
	            ClientHttpResponse response = execution.execute(request,body);
	            response.getHeaders().setContentType(MediaType.APPLICATION_XML);
	            return response;
	        });
		return restTemplate;
	}
	
}
