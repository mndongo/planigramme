package com.epcc.signaletique.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@XmlType(name = "event")
public class Event implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Integer activityId;
	private Integer session;
	private Integer repetition;
	private String name;
	private String endHour;
	private String  startHour;
	private String date;
	private Integer absoluteSlot;
	private Integer slot;
	private Integer day;
	private Integer week;
	private Integer additionalResources;
	private Integer duration;
	private String info;
	private String note;
	private String color;
	private String isLockPosition;
	private Integer oldDuration;
	private Integer oldSlot;
	private Integer oldDay;
	private Integer oldWeek;
	private String lastUpdate;
	private String creation;
	private boolean isLockResources;
	private Boolean isSoftKeepResources;
	private Ressources ressources;
	
	public Event() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Event(List<Event> events, Long id, Integer activityId, Integer session, Integer repetition, String name,
			String endHour, String startHour, String date, Integer absoluteSlot, Integer slot, Integer day, Integer week,
			Integer additionalResources, Integer duration, String info, String note, String color,
			String isLockPosition, Integer oldDuration, Integer oldSlot, Integer oldDay, Integer oldWeek,
			String lastUpdate, String creation, boolean isLockResources, Boolean isSoftKeepResources,
			Ressources ressources) {
		super();
		this.id = id;
		this.activityId = activityId;
		this.session = session;
		this.repetition = repetition;
		this.name = name;
		this.endHour = endHour;
		this.startHour = startHour;
		this.date = date;
		this.absoluteSlot = absoluteSlot;
		this.slot = slot;
		this.day = day;
		this.week = week;
		this.additionalResources = additionalResources;
		this.duration = duration;
		this.info = info;
		this.note = note;
		this.color = color;
		this.isLockPosition = isLockPosition;
		this.oldDuration = oldDuration;
		this.oldSlot = oldSlot;
		this.oldDay = oldDay;
		this.oldWeek = oldWeek;
		this.lastUpdate = lastUpdate;
		this.creation = creation;
		this.isLockResources = isLockResources;
		this.isSoftKeepResources = isSoftKeepResources;
		this.ressources = ressources;
	}
	public Long getId() {
		return id;
	}
	@XmlAttribute
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getActivityId() {
		return activityId;
	}
	@XmlAttribute
	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}
	public Integer getSession() {
		return session;
	}
	@XmlAttribute
	public void setSession(Integer session) {
		this.session = session;
	}
	public Integer getRepetition() {
		return repetition;
	}
	@XmlAttribute
	public void setRepetition(Integer repetition) {
		this.repetition = repetition;
	}
	public String getName() {
		return name;
	}
	@XmlAttribute
	public void setName(String name) {
		this.name = name;
	}
	public String getEndHour() {
		return endHour;
	}
	@XmlAttribute
	public void setEndHour(String endHour) {
		this.endHour = endHour;
	}
	public String getStartHour() {
		return startHour;
	}
	@XmlAttribute
	public void setStartHour(String startHour) {
		this.startHour = startHour;
	}
	public String getDate() {
		return date;
	}
	@XmlAttribute
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getAbsoluteSlot() {
		return absoluteSlot;
	}
	@XmlAttribute
	public void setAbsoluteSlot(Integer absoluteSlot) {
		this.absoluteSlot = absoluteSlot;
	}
	public Integer getSlot() {
		return slot;
	}
	@XmlAttribute
	public void setSlot(Integer slot) {
		this.slot = slot;
	}
	public Integer getDay() {
		return day;
	}
	@XmlAttribute
	public void setDay(Integer day) {
		this.day = day;
	}
	public Integer getWeek() {
		return week;
	}
	@XmlAttribute
	public void setWeek(Integer week) {
		this.week = week;
	}
	public Integer getAdditionalResources() {
		return additionalResources;
	}
	@XmlAttribute
	public void setAdditionalResources(Integer additionalResources) {
		this.additionalResources = additionalResources;
	}
	public Integer getDuration() {
		return duration;
	}
	@XmlAttribute
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public String getInfo() {
		return info;
	}
	@XmlAttribute
	public void setInfo(String info) {
		this.info = info;
	}
	public String getNote() {
		return note;
	}
	@XmlAttribute
	public void setNote(String note) {
		this.note = note;
	}
	public String getColor() {
		return color;
	}
	@XmlAttribute
	public void setColor(String color) {
		this.color = color;
	}
	public String getIsLockPosition() {
		return isLockPosition;
	}
	@XmlAttribute
	public void setIsLockPosition(String isLockPosition) {
		this.isLockPosition = isLockPosition;
	}
	public Integer getOldDuration() {
		return oldDuration;
	}
	@XmlAttribute
	public void setOldDuration(Integer oldDuration) {
		this.oldDuration = oldDuration;
	}
	public Integer getOldSlot() {
		return oldSlot;
	}
	@XmlAttribute
	public void setOldSlot(Integer oldSlot) {
		this.oldSlot = oldSlot;
	}
	public Integer getOldDay() {
		return oldDay;
	}
	@XmlAttribute
	public void setOldDay(Integer oldDay) {
		this.oldDay = oldDay;
	}
	public Integer getOldWeek() {
		return oldWeek;
	}
	@XmlAttribute
	public void setOldWeek(Integer oldWeek) {
		this.oldWeek = oldWeek;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	@XmlAttribute
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getCreation() {
		return creation;
	}
	@XmlAttribute
	public void setCreation(String creation) {
		this.creation = creation;
	}
	public boolean isLockResources() {
		return isLockResources;
	}
	@XmlAttribute
	public void setLockResources(boolean isLockResources) {
		this.isLockResources = isLockResources;
	}
	public Boolean getIsSoftKeepResources() {
		return isSoftKeepResources;
	}
	@XmlAttribute
	public void setIsSoftKeepResources(Boolean isSoftKeepResources) {
		this.isSoftKeepResources = isSoftKeepResources;
	}
	public Ressources getRessources() {
		return ressources;
	}
	@XmlElement(name="resources")
	public void setRessources(Ressources ressources) {
		this.ressources = ressources;
	}
	@Override
	public String toString() {
		return "Event [id=" + id + ", activityId=" + activityId + ", session="
				+ session + ", repetition=" + repetition + ", name=" + name + ", endHour=" + endHour + ", startHour="
				+ startHour + ", date=" + date + ", absoluteSlot=" + absoluteSlot + ", slot=" + slot + ", day=" + day
				+ ", week=" + week + ", additionalResources=" + additionalResources + ", duration=" + duration
				+ ", info=" + info + ", note=" + note + ", color=" + color + ", isLockPosition=" + isLockPosition
				+ ", oldDuration=" + oldDuration + ", oldSlot=" + oldSlot + ", oldDay=" + oldDay + ", oldWeek="
				+ oldWeek + ", lastUpdate=" + lastUpdate + ", creation=" + creation + ", isLockResources="
				+ isLockResources + ", isSoftKeepResources=" + isSoftKeepResources + ", ressources=" + ressources + "]";
	}
	
	
	
	
	
}
