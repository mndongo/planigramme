package com.epcc.signaletique.model;

import java.util.Date;
import java.util.List;

public class Cours {

	public Cours() {
		// TODO Auto-generated constructor stub
	}

private Long idEvent;
	
	private Integer activityId;
	
	private String nomActivite;
	
	private Integer session;
	
	private Integer repetition;
	
	private String formationNom;
	
	private String endHour;
	
	private String  startHour;
	
	private String heurDebutCours;
	
	private String setHeurFinCours;
	
	private String minute;
	
	private String date;
	
	private Integer absoluteSlot;
	
	private Integer slot;
	
	private Integer day;
	
	private Integer week;
	
	private Integer additionalResources;
	
	private Integer duration;
	
	private String infoSalle;
	
	private List<String> listNumeroSalle;
	
	private String note;
	
	private String color;
	
	private String isLockPosition;
	
	private Integer oldDuration;
	
	private Integer oldSlot;
	
	private Integer oldDay;
	
	private Integer oldWeek;
	
	private Date lastUpdate;
	
	private String creation;
	
	private boolean isLockResources;
	
	private Boolean isSoftKeepResources;
	
	private String batimentName;
	
	private String codeX;
	
	private String categ;

	public Long getIdEvent() {
		return idEvent;
	}

	public void setIdEvent(Long idEvent) {
		this.idEvent = idEvent;
	}

	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public String getNomActivite() {
		return nomActivite;
	}

	public void setNomActivite(String nomActivite) {
		this.nomActivite = nomActivite;
	}

	public Integer getSession() {
		return session;
	}

	public void setSession(Integer session) {
		this.session = session;
	}

	public Integer getRepetition() {
		return repetition;
	}

	public void setRepetition(Integer repetition) {
		this.repetition = repetition;
	}

	public String getFormationNom() {
		return formationNom;
	}

	public void setFormationNom(String formationNom) {
		this.formationNom = formationNom;
	}

	public String getEndHour() {
		return endHour;
	}

	public void setEndHour(String endHour) {
		this.endHour = endHour;
	}

	public String getStartHour() {
		return startHour;
	}

	public void setStartHour(String startHour) {
		this.startHour = startHour;
	}

	public String getHeurDebutCours() {
		return heurDebutCours;
	}

	public void setHeurDebutCours(String heurDebutCours) {
		this.heurDebutCours = heurDebutCours;
	}

	public String getSetHeurFinCours() {
		return setHeurFinCours;
	}

	public void setSetHeurFinCours(String setHeurFinCours) {
		this.setHeurFinCours = setHeurFinCours;
	}

	public String getMinute() {
		return minute;
	}

	public void setMinute(String minute) {
		this.minute = minute;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getAbsoluteSlot() {
		return absoluteSlot;
	}

	public void setAbsoluteSlot(Integer absoluteSlot) {
		this.absoluteSlot = absoluteSlot;
	}

	public Integer getSlot() {
		return slot;
	}

	public void setSlot(Integer slot) {
		this.slot = slot;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getWeek() {
		return week;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

	public Integer getAdditionalResources() {
		return additionalResources;
	}

	public void setAdditionalResources(Integer additionalResources) {
		this.additionalResources = additionalResources;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getInfoSalle() {
		return infoSalle;
	}

	public void setInfoSalle(String infoSalle) {
		this.infoSalle = infoSalle;
	}

	public List<String> getListNumeroSalle() {
		return listNumeroSalle;
	}

	public void setListNumeroSalle(List<String> listNumeroSalle) {
		this.listNumeroSalle = listNumeroSalle;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIsLockPosition() {
		return isLockPosition;
	}

	public void setIsLockPosition(String isLockPosition) {
		this.isLockPosition = isLockPosition;
	}

	public Integer getOldDuration() {
		return oldDuration;
	}

	public void setOldDuration(Integer oldDuration) {
		this.oldDuration = oldDuration;
	}

	public Integer getOldSlot() {
		return oldSlot;
	}

	public void setOldSlot(Integer oldSlot) {
		this.oldSlot = oldSlot;
	}

	public Integer getOldDay() {
		return oldDay;
	}

	public void setOldDay(Integer oldDay) {
		this.oldDay = oldDay;
	}

	public Integer getOldWeek() {
		return oldWeek;
	}

	public void setOldWeek(Integer oldWeek) {
		this.oldWeek = oldWeek;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getCreation() {
		return creation;
	}

	public void setCreation(String creation) {
		this.creation = creation;
	}

	public boolean isLockResources() {
		return isLockResources;
	}

	public void setLockResources(boolean isLockResources) {
		this.isLockResources = isLockResources;
	}

	public Boolean getIsSoftKeepResources() {
		return isSoftKeepResources;
	}

	public void setIsSoftKeepResources(Boolean isSoftKeepResources) {
		this.isSoftKeepResources = isSoftKeepResources;
	}

	public String getBatimentName() {
		return batimentName;
	}

	public void setBatimentName(String batimentName) {
		this.batimentName = batimentName;
	}

	public String getCodeX() {
		return codeX;
	}

	public void setCodeX(String codeX) {
		this.codeX = codeX;
	}

	public String getCateg() {
		return categ;
	}

	public void setCateg(String categ) {
		this.categ = categ;
	}

}
