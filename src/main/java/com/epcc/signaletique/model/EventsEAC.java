package com.epcc.signaletique.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "events")
@XmlAccessorType(XmlAccessType.NONE)
public class EventsEAC {

	private List<Event> lEvent = new ArrayList<Event>();

	public EventsEAC() {
		super();
	}

	public EventsEAC(List<Event> lEvent) {
		super();
		this.lEvent = lEvent;
	}

	public List<Event> getlEvent() {
		return lEvent;
	}
	@XmlElement(name="event")
	public void setlEvent(List<Event> lEvent) {
		this.lEvent = lEvent;
	}


}
