package com.epcc.signaletique.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "resource")
public class Ressource implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean fromWorkflow;
	private Integer nodeId;
	private Integer nodeOrId;
	private Integer quantity;
	private String category;
	private String name;
	private Integer id;
	
	
	public boolean isFromWorkflow() {
		return fromWorkflow;
	}

	public void setFromWorkflow(boolean fromWorkflow) {
		this.fromWorkflow = fromWorkflow;
	}

	public Integer getNodeId() {
		return nodeId;
	}
	
	@XmlAttribute
	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}
	public Integer getNodeOrId() {
		return nodeOrId;
	}

	@XmlAttribute
	public void setNodeOrId(Integer nodeOrId) {
		this.nodeOrId = nodeOrId;
	}
	public Integer getQuantity() {
		return quantity;
	}

	@XmlAttribute
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getCategory() {
		return category;
	}

	@XmlAttribute
	public void setCategory(String category) {
		this.category = category;
	}
	public String getName() {
		return name;
	}

	@XmlAttribute
	public void setName(String name) {
		this.name = name;
	}
	public Integer getId() {
		return id;
	}

	@XmlAttribute
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Ressource [fromWorkflow=" + fromWorkflow + ", nodeId=" + nodeId + ", nodeOrId=" + nodeOrId
				+ ", quantity=" + quantity + ", category=" + category + ", name=" + name + ", id=" + id + "]";
	}
	
}
